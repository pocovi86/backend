package com.backend.app.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "users")
@Access(AccessType.FIELD)
public class User extends ParentEntity{

    private static final long serialVersionUID = 3773035504987942980L;
    @Column(name = "first_name", nullable = false, length = 255)
    private String firstName;
    @Column(name = "second_name", nullable = true, length = 255)
    private String secondName;
    @Column(name = "first_surname", nullable = false, length = 255)
    private String firstSurname;
    @Column(name = "second_surname", nullable = true, length = 255)
    private String secondSurname;
    @Column(name = "phone", nullable = true, length = 30)
    private String phone;
    @Column(name = "address", nullable = false, length = 150)
    private String address;
}
