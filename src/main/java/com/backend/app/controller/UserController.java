package com.backend.app.controller;

import com.backend.app.implement.UserService;
import com.backend.app.model.User;
import com.backend.app.util.RestResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class UserController {

    @Autowired
    UserService userService;
    private ObjectMapper mapper = new ObjectMapper();

    @PostMapping("/addUser")
    public RestResponse save(@RequestBody String userJson) throws JsonProcessingException {
        User user = this.mapper.readValue(userJson, User.class);
        if (!this.validate(user)){
            return new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(), "Los campos obligatorios no están diligenciados");
        }
        this.userService.save(user);
        return new RestResponse(HttpStatus.OK.value(), "Operacion Exitosa!");
    }

    @PutMapping("/putUser")
    public RestResponse update(@RequestBody String userJson) throws JsonProcessingException{
        User user = this.mapper.readValue(userJson, User.class);
        if (!this.validate(user)){
            return new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(), "Los campos obligatorios no están diligenciados");
        }
        this.userService.save(user);
        return new RestResponse(HttpStatus.OK.value(), "Usuario actualizado con exito!");
    }

    @GetMapping("/users")
    public List<User> getUsers(){
        return this.userService.findAll();
    }

    @DeleteMapping("/deleteUser")
    public void deleteUser(@RequestBody String userJson) throws Exception {
        User user = this.mapper.readValue(userJson, User.class);
        if (user.getId() == null){
            throw new Exception("User not existent");
        }
        this.userService.deleteUser(user.getId());
    }

    private boolean validate(User user){
        boolean isValid = true;
        if (user.getFirstName() == null){
            isValid = false;
        }
        if (user.getFirstSurname() == null){
            isValid = false;
        }
        if (user.getAddress() == null){
            isValid = false;
        }
        return isValid;
    }
}
