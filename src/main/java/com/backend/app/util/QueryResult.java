package com.backend.app.util;

import lombok.Data;

import java.util.List;

@Data
public class QueryResult {
    private int totalRecords;
    private List<Object> list;
}
