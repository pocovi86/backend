package com.backend.app.implement;

import com.backend.app.model.User;

import java.util.List;

public interface UserService {
    User save(User user);
    List<User> findAll();
    void deleteUser(Long id);
}
